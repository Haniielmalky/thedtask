//
//  MainVC.swift
//  TheDTask
//
//  Created by haniielmalky on 11/9/18.
//  Copyright © 2018 Hani Elmalky. All rights reserved.
//

import UIKit
import Kingfisher
import ObjectMapper
import CoreData

class MainVC: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  
  var itemList = [Item]()
  var labelViewHeight : CGFloat?
  var selectedItem : Item?
  
  // MARK: - ViewDidLoad
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    getProducts()
    tableView.backgroundColor = .clear
    
  }
  
  
  
  // MARK: - Navigation
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if(segue.identifier == "details"){
      let detailsVC = segue.destination as! ItemDetailsVC
      if(selectedItem != nil){
        detailsVC.item = selectedItem
      }
    }
  }
  
  
}

// MARK: - Table view data source and delegate

extension MainVC: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return itemList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MainTVCell
    
    cell.backgroundColor = .clear
    cell.cellMainView.layer.cornerRadius = 12
    cell.cellMainView.clipsToBounds = true
    
    let item = itemList[indexPath.row]
    
    if(item.imageLink != nil){
      if let downloadURL = URL(string: item.imageLink!){
        cell.itemImageView.kf.setImage(with: downloadURL)
      }
    }
    if(item.name != nil){
      cell.itemTitleLabel.text = item.name
    }
    
    if(item.price != nil){
      cell.itemPriceLabel.text = "\(String(describing: item.price!))"
    }
    
    // image view height = ( image view width * the item image height) / item image width
    if(item.imageHeight != nil){
      let height = (((self.view.frame.width - 52)*CGFloat(Int(item.imageHeight!)!)) / CGFloat(integerLiteral: Int(item.imageWidth!)!))
      cell.itemImageViewHeight.constant = height
    }
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    selectedItem = itemList[indexPath.row]
    performSegue(withIdentifier: "details", sender: nil)
    tableView.deselectRow(at: indexPath, animated: false)
  }
  
}

// MARK: - data requests

extension MainVC {
  func getProducts(){
    DispatchQueue.main.async {
      
      self.view.lock()
      
      let manager = Manager()
      manager.perform(){ (JSON, String) -> Void in
        
        if String != nil {
          self.showAlertWithTitle(title: "Warning", message: String!, type: .error)
        }else if(JSON!["data"] != nil) {
          
          let itemArray: [Item] = Mapper<Item>().mapArray(JSONObject: JSON!["data"]!)!
          self.itemList = itemArray
          
          self.tableView.reloadData()
          
        }
        self.view.unlock()
        
      }
    }
    
  }
  
}
