//
//  MainTVCell.swift
//  TheDTask
//
//  Created by haniielmalky on 11/9/18.
//  Copyright © 2018 Hani Elmalky. All rights reserved.
//

import UIKit

class MainTVCell: UITableViewCell {
  
  @IBOutlet weak var labelsView: UIView!
  @IBOutlet weak var cellMainView: UIView!
  @IBOutlet weak var itemTitleLabel: UILabel!
  @IBOutlet weak var itemPriceLabel: UILabel!
  @IBOutlet weak var itemImageView: UIImageView!
  @IBOutlet weak var itemImageViewHeight: NSLayoutConstraint!
  
}
