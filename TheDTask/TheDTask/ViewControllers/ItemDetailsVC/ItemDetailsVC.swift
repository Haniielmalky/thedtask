//
//  ItemDetailsVC.swift
//  TheDTask
//
//  Created by haniielmalky on 11/9/18.
//  Copyright © 2018 Hani Elmalky. All rights reserved.
//

import UIKit
import Kingfisher

class ItemDetailsVC: UIViewController {
  
  @IBOutlet weak var itemTitle: UINavigationItem!
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var imageViewHeight: NSLayoutConstraint!
  @IBOutlet weak var descriptionTV: UITextView!
  @IBOutlet weak var priceLabel: UILabel!
  
  var item : Item?
  
  // MARK: - viewDidLoad
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if(item != nil){
      setUpView()
    }else{
      showAlertWithTitle(title: "Warning", message: "Something Went wrong", type: .error)
    }
    
  }

  // MARK: - setUpView
  
  func setUpView(){
    if(item!.imageLink != nil){
      if let downloadURL = URL(string: item!.imageLink!){
        imageView.kf.setImage(with: downloadURL)
      }
    }
    
    if(item!.name != nil){
      itemTitle.title = item!.name! + "Details"
    }
    
    if(item!.price != nil){
      priceLabel.text = "Price: \(String(describing: item!.price!))"
    }
    
    if(item!.imageHeight != nil){

      // image view height = ( image view width * the item image height) / item image width
      let height = (((self.view.frame.width - 50)*CGFloat(Int(item!.imageHeight!)!)) / CGFloat(integerLiteral: Int(item!.imageWidth!)!))
      imageViewHeight.constant = height
    }
    
    if(item!.productDescription != nil){
      descriptionTV.text = item!.productDescription
    }
  }
}
