//
//  Manager.swift
//  TheDTask
//
//  Created by haniielmalky on 11/9/18.
//  Copyright © 2018 Hani Elmalky. All rights reserved.
//

import Alamofire

// MARK: - Base URL

let hostName = "https://limitless-forest-98976.herokuapp.com"

class Manager {
  
  // MARK: - perform request
  func perform(methodType: HTTPMethod = .get, useCustomeURL: Bool = false, urlStr: String = "", parameters: [String: AnyObject]? = nil, completionHandler: @escaping (AnyObject?, String?) -> Void)-> Void {
    
    let urlString = hostName
    
    
    Alamofire.request(urlString, method: methodType, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON{ response in
      
      debugPrint(response)
      
      if let statusCode = response.response?.statusCode
      {
        if statusCode >= 200 && statusCode <= 300 {
          // success
          if(response.result.value != nil){
            let dict = response.result.value!
            completionHandler(dict as AnyObject, nil)
          }else {
            completionHandler( "success" as AnyObject , nil)
          }
          
        }else{
          // failer
          if response.result.value != nil {
            let dict = response.result.value as! NSDictionary
            let error = dict.object(forKey: "Message") as! String
            completionHandler(nil, error)
          }else{
            completionHandler(nil, "Something Went Wrong.")
          }
          
        }
      }
    }
  }
}
