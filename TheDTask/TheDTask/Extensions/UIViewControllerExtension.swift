//
//  UIViewControllerExtension.swift
//  TheDTask
//
//  Created by haniielmalky on 11/9/18.
//  Copyright © 2018 Hani Elmalky. All rights reserved.
//

import UIKit

extension UIViewController {
  
  @IBAction func backClicked(_ sender: Any) {
    _ = self.navigationController?.popViewController(animated: true)
  }
  
  func showAlertWithTitle(title: String, message: String, type: ISAlertType, duration : Double = 3.5) {
    ISMessages.showCardAlert(withTitle: title, message: message, duration: duration, hideOnSwipe: true, hideOnTap: true, alertType: type, alertPosition: .top, didHide: nil)
  }
}
