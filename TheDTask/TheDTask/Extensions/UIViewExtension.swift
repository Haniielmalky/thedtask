
//
//  UIViewExtensio.swift
//  TheDTask
//
//  Created by haniielmalky on 11/9/18.
//  Copyright © 2018 Hani Elmalky. All rights reserved.
//

import UIKit

extension UIView {
  
  // MARK: - lock view
  
  func lock(frameRect: CGRect = CGRect.zero) {
    if (viewWithTag(10) != nil) {
      //View is already locked
    }
    else {
      // view not locked
      let lockView = UIView()
      
      if frameRect == CGRect.zero{
        lockView.frame = bounds
      }
      else{
        lockView.frame = frameRect
      }
      
      lockView.backgroundColor = UIColor(white: 0.5, alpha: 0.3)
      lockView.tag = 10
      lockView.alpha = 0.0
      
      let activityIndicator = UIActivityIndicatorView(frame:CGRectMake(lockView.center.x,lockView.center.y, 50, 50))
      
      activityIndicator.style = .whiteLarge
      activityIndicator.center.x = lockView.center.x
      activityIndicator.center.y = lockView.center.y
      activityIndicator.color = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
      
      activityIndicator.startAnimating()
      lockView.addSubview(activityIndicator)
      addSubview(lockView)
      
      UIView.animate(withDuration: 0.2) {
        lockView.alpha = 1.0
      }
    }
  }
  
  // MARK: - unlock view
  
  func unlock() {
    if let lockView = self.viewWithTag(10) {
      UIView.animate(withDuration: 0.2, animations: {
        lockView.alpha = 0.0
      }) { finished in
        lockView.removeFromSuperview()
      }
    }
  }
  
  func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
    return CGRect(x: x, y: y, width: width, height: height)
  }
  
  // MARK: - custome round corners
  
  func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
    let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    self.layer.mask = mask
  }
  
}

