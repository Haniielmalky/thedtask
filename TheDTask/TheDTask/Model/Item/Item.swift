//
//  Item.swift
//  TheDTask
//
//  Created by haniielmalky on 11/9/18.
//  Copyright © 2018 Hani Elmalky. All rights reserved.
//

import ObjectMapper

struct Item: Mappable {
  
  var id : Int?
  var name : String?
  var productDescription : String?
  var imageLink : String?
  var imageHeight : String?
  var imageWidth : String?
  var price : Int?
  var imageData : Data?
  
  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    
    id                  <- map["id"]
    name                <- map["name"]
    productDescription  <- map["productDescription"]
    imageLink           <- map["image.link"]
    imageHeight         <- map["image.height"]
    imageWidth          <- map["image.width"]
    price               <- map["price"]
    
  }
  
}
